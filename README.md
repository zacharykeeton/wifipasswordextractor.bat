# wifipasswordextractor.bat
 Extract all known SSIDs and cleartext wifi passwords from a windows machine. — 

A windows batch program to extract known wifi ssids and passwords from windows.
Outputs a file called 'ssidswkeys.txt' in the same directory with all the Wifi SSIDs that the computer
remembers connecting to along with clear-text passwords if found.

Zachary Keeton August 2015 - @ZacharyKeeton.  Inspired by the work of Aditya K Sood  (adi_ks [at] secniche.org)


note: to do the same in Linux, just run (must be able to sudo):

```bash
sudo grep psk= /etc/NetworkManager/system-connections/* | cut -d '/' -f5 | sed 's/psk=//' > ssidswkeys
```
